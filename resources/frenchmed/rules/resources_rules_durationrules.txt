//******
// Copyright LIMSI-CNRS
// Author: Véronique Moriceau
// E-mail : moriceau@limsi.fr
// Date: 2013-05-27
// Governed by the CeCILL license under French law and abiding by the rules of distribution of free software.
// You can use, modify and/or redistribute the software under the terms of the CeCILL license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info/licences/Licence_CeCILL_V1.1-US.txt".
//******
// This file contains rules for the temporal expressions of the type duration: durationrules
// RULENAME=""",EXTRACTION="",NORM_VALUE=""(,OFFSET="")?(,POS_CONSTRAINT="")?(,NORM_MOD="")?(,NORM_QUANT="")?(,NORM_FREQ="")?



/////////////////////
// POSITIVE RULES // 
/////////////////////

// duration_r1
// EXAMPLE r1a-1: moins de soixante jours
RULENAME="duration_r1a",EXTRACTION="(%reApproximate )?(%reNumWord2D|%reNumWord1D)( |-)%reUnit",NORM_VALUE="P%normDurationNumber(group(3))%normUnit4Duration(group(7))",NORM_MOD="%normApprox4Durations(group(2))"

// EXAMPLE r1b-1: moins de 60 jours
RULENAME="duration_r1b",EXTRACTION="(%reApproximate )?([\d]+)( |-)%reUnit",NORM_VALUE="Pgroup(3)%normUnit4Duration(group(5))",NORM_MOD="%normApprox4Durations(group(2))"

// EXAMPLE r1c-1: moins de soixante minutes
RULENAME="duration_r1c",EXTRACTION="(%reApproximate )?(%reNumWord2D|%reNumWord1D)( |-)(minutes?|heures?)",NORM_VALUE="PT%normDurationNumber(group(3))%normUnit4Duration(group(7))",NORM_MOD="%normApprox4Durations(group(2))"

// EXAMPLE r1d-1: moins de 60 minutes
RULENAME="duration_r1d",EXTRACTION="(%reApproximate )?([\d]+)( |-)(minutes?|heures?)",NORM_VALUE="PTgroup(3)%normUnit4Duration(group(5))",NORM_MOD="%normApprox4Durations(group(2))"



// duration_r2
// EXAMPLE r2a-1: les vingt dernières/prochaines années
RULENAME="duration_r2a",EXTRACTION="les (%reNumWord2D|%reNumWord1D) %reThisNextLast %reUnit",NORM_VALUE="P%normDurationNumber(group(1))%normUnit4Duration(group(5))"

// EXAMPLE r2b-1: les 20 dernières/prochaines années
RULENAME="duration_r2b",EXTRACTION="les ([\d]+) %reThisNextLast %reUnit",NORM_VALUE="P%normDurationNumber(group(1))%normUnit4Duration(group(3))"     



//////////////////////
// FUZZY UNIT RULES //
//////////////////////

// duration_r4
// EXAMPLE r4a-1: plusieurs jours
RULENAME="duration_r4a",EXTRACTION="(%reApproximate )?([Pp]lusieurs|[Qq]uelques) %reUnit",NORM_VALUE="PX%normUnit4Duration(group(4))",NORM_MOD="%normApprox4Durations(group(2))"

// EXAMPLE r4b-1: plusieurs minutes
RULENAME="duration_r4b",EXTRACTION="(%reApproximate )?([Pp]lusieurs|[Qq]uelques) (minutes?|heures?)",NORM_VALUE="PTX%normUnit4Duration(group(4))",NORM_MOD="%normApprox4Durations(group(2))"

// EXAMPLE r4c-1: les années suivantes
RULENAME="duration_r4c",EXTRACTION="[CcLlDd]es (quelques )?%reUnit %reThisNextLast",NORM_VALUE="PX%normUnit4Duration(group(2))"

// EXAMPLE r4d-1: ces dernières années
RULENAME="duration_r4d",EXTRACTION="[CcLlDd]es (quelques )?%reThisNextLast %reUnit",NORM_VALUE="PX%normUnit4Duration(group(3))"

// EXAMPLE r4e-1: les dernières heures
RULENAME="duration_r4e",EXTRACTION="[CcLlDd]es (quelques )?%reThisNextLast (minutes?|heures?)",NORM_VALUE="PTX%normUnit4Duration(group(3))"



// duration_r5
// EXAMPLE r5a-1: deux à/et trois ans
RULENAME="duration_r5a",EXTRACTION="(%reNumWord2D|%reNumWord1D)%reAndOrTo(%reNumWord2D|%reNumWord1D) %reUnit",NORM_VALUE="P%normDurationNumber(group(1))%normUnit4Duration(group(8))",OFFSET="group(1)-group(1)"

// EXAMPLE r5b-1: deux à/et trois heures
RULENAME="duration_r5b",EXTRACTION="(%reNumWord2D|%reNumWord1D)%reAndOrTo(%reNumWord2D|%reNumWord1D) (minutes?|heures?)",NORM_VALUE="PT%normDurationNumber(group(1))%normUnit4Duration(group(8))",OFFSET="group(1)-group(1)"

// EXAMPLE r5c-1: 2 à/et 3 ans
RULENAME="duration_r5c",EXTRACTION="([\d]+)%reAndOrTo([\d]+) %reUnit",NORM_VALUE="Pgroup(1)%normUnit4Duration(group(4))",OFFSET="group(1)-group(1)"

// EXAMPLE r5d-1: 2 à/et 3 heures
RULENAME="duration_r5d",EXTRACTION="([\d]+)%reAndOrTo([\d]+) (minutes?|heures?)",NORM_VALUE="PTgroup(1)%normUnit4Duration(group(4))",OFFSET="group(1)-group(1)"



// duration_r6
// EXAMPLE r6a: toute la semaine, tout le mois, toute l'année
RULENAME="duration_r6a",EXTRACTION="[Tt]oute? (la |le |l'|ce |cette )%reUnit",NORM_VALUE="P1%normUnit4Duration(group(2))"

// EXAMPLE r6b_1: 24 heures sur 24
RULENAME="duration_r6b",EXTRACTION="(24) (heures) sur 24",NORM_VALUE="PT%normDurationNumber(group(1))%normUnit4Duration(group(2))" 

// EXAMPLE r6c_1: 7 jours sur 7
RULENAME="duration_r6c",EXTRACTION="(7) (jours) sur 7",NORM_VALUE="P%normDurationNumber(group(1))%normUnit4Duration(group(2))" 



// jusqu 'à présent : durée
RULENAME="duration_r8a",EXTRACTION="jusqu ?' ?[aà] (présent|aujourd ?' ?|maintenant)",NORM_VALUE="UNDEF"
RULENAME="duration_r8b",EXTRACTION="jusqu ?' ?[aà]u? (lendemain|demain)",NORM_VALUE="UNDEF"
RULENAME="duration_r8c",EXTRACTION="jusqu ?' ?[aà] (hier)",NORM_VALUE="UNDEF"
// jusqu 'à cicatrisation complète : durée
RULENAME="duration_r8d",EXTRACTION="jusqu ?' ?[aà] (cicatrisation|rémission|guérison) complète",NORM_VALUE="UNDEF"

RULENAME="duration_r8e",EXTRACTION="([Pp]endant|[Dd]urant|[Aa]u cours de) (l ?'|son|ses) ?(hospitalisations?|opérations?|examens?|analyses?|traitements?)",NORM_VALUE="UNDEF"
RULENAME="duration_r8f",EXTRACTION="([Pp]endant les?|[Dd]urant les?|[Aa]u cours du|[Aa]u cours des?) traitements?",NORM_VALUE="UNDEF"



// NEGATIVE RULES have to be located here (LREC rules)
RULENAME="date_lrec0a_negative",EXTRACTION="[0]+",NORM_VALUE="REMOVE"
//RULENAME="date_lrec0b_negative",EXTRACTION="%reYearBC (\b\S+s\b)",NORM_VALUE="REMOVE",POS_CONSTRAINT="group(2):NC:"
//RULENAME="date_lrec0c_negative",EXTRACTION="%reYearBC (\b\S+s\b)",NORM_VALUE="REMOVE",POS_CONSTRAINT="group(2):NAM:"
//RULENAME="date_lrec0d_negative",EXTRACTION="%reYearBC(%| %)",NORM_VALUE="REMOVE"
RULENAME="date_lrec0e_negative",EXTRACTION="%reYearBC (km\b|cm\b|mm\b|m\b|kg\b)",NORM_VALUE="REMOVE"
RULENAME="date_lrec0f_negative",EXTRACTION="(\btotal) (de) %reYearBC",NORM_VALUE="REMOVE"
//RULENAME="date_lrec0g_negative",EXTRACTION="%reYearBC ([\S]+)",NORM_VALUE="REMOVE",POS_CONSTRAINT="group(2):ACRNM:"
RULENAME="date_lrec0h_negative",EXTRACTION="%reYearBC ([\S]+)",NORM_VALUE="REMOVE",POS_CONSTRAINT="group(2):NUM:"
